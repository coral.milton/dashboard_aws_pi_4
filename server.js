const express = require('express');
const path = require('path');
const cors = require('cors');
const AWS = require('aws-sdk');
const fs = require('fs');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const app = express();
const port = 3000;

// Serve static files from the "public" directory
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
// Parse incoming request bodies in a middleware before your handlers


AWS.config.update({
  accessKeyId: 'AKIAQ3EGPGCNZSNY2RZI',
  secretAccessKey: 'BhSFzRMVlD4fa+AZyvNCYrvL+g9P93H86puoUPz/',
  region: 'us-east-1'
});





const elasticbeanstalk = new AWS.ElasticBeanstalk();

// Set up Cognito user pool data
const poolData = {
  UserPoolId: 'us-east-1_zVwIB7RBF',
  ClientId: '6ao9m933gi4eeg8fen2rj30alf',
};

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);


app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'login.html'));
});



app.get('/confirmar', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/confirmacion/confirmacion.html'));
});



app.post('/login', (req, res) => {

  const email = req.body.email;
  const password = req.body.password;

  const authenticationData = {
    Username: email,
    Password: password,
  };

  const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);



  const userData = {
    Username: email,
    Pool: userPool,
  };

  const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

  cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess: function (result) {
      console.log('Authentication successful');
      usersub = "user-" + result.getIdToken().payload.sub.substring(0, 20);  ///  longitud sub
      user=email;
      const params = {
        ApplicationName: 'back_end_api_pi',
        EnvironmentNames: [usersub]
      };

      elasticbeanstalk.describeEnvironments(params, function (err, data) {
        if (err) {
          console.error('Failed to get environment details:', err);
        } else {
          // Assuming the environment details are found
          if (data.Environments && data.Environments.length > 0) {
            const environment = data.Environments[0];
            //console.log('Environment URL:', environment.EndpointURL);
            //res.sendFile(path.join(__dirname, 'public/index/index.html'));

            fs.readFile(path.join(__dirname, 'public/index/index.html'), { encoding: 'utf-8' }, (err, html) => {
              if (err) {
                res.status(500).send('Error reading HTML file.');
              }
              else {

                if (environment.Health=="Green") {
                  const datos = html.replace('<a href="#" class="d-block">Alexander Pierce</a>', `<a href="#" class="d-block">${user}</a>`);
                  const modifiedHtml = `<script>alert("Se ha asignado la siguiente direccion para su Rassperry: http://${environment.EndpointURL}/");</script>` + `<script>const api="http://${environment.EndpointURL}/";</script>` + datos;
                  
                  res.send(modifiedHtml);
                }
                else {
                  res.send('<script>alert("El entorno aun no ha sido creado esperar un momento");</script>');
                }

              }

            });

          }
          
        }
      });

    },
    onFailure: function (err) {
      res.sendFile(path.join(__dirname, 'login.html'));
    },
  });

});


app.post('/register', (req, res) => {
  const { email, password } = req.body;

  const attributeList = [];
  const dataEmail = {
    Name: 'email',
    Value: email
  };

  const attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
  attributeList.push(attributeEmail);

  userPool.signUp(email, password, attributeList, null, function (err, result) {
    if (err) {
      res.status(400).send(err.message);
    }
    else {
      const cognitoUser = result.user;
      sub = result.userSub;
      res.sendFile(path.join(__dirname, 'public/confirmacion/confirmacion.html'));  // cambiar aqui

      app.post('/confirmRegistration', (req, res) => {
        const { email, code } = req.body;

        const userData = {
          Username: email,
          Pool: userPool
        };

        const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

        cognitoUser.confirmRegistration(code, true, function (err, result) {
          if (err) {
            res.status(400).send(err.message);
          }
          else { // Inicio else 

            userSub = sub.substring(0, 20); // longitud sub

            console.log('Confirmation successful');


            elasticbeanstalk.describeConfigurationSettings({
              ApplicationName: 'back_end_api_pi',
              EnvironmentName: 'Backendapipi-env'
            }, function (err, configData) {
              if (err) {

                console.error('Failed to get environment configuration:', err);

              }
              else {

                settings = configData.ConfigurationSettings[0].OptionSettings;

                //console.log(settings)
                elasticbeanstalk.createEnvironment({
                  ApplicationName: 'back_end_api_pi',
                  EnvironmentName: `user-${userSub}`,
                  TemplateName: 'basico',
                  VersionLabel: "code-pipeline-1717046020700-0c7a0444bde0a27402830e1682c2a50ec65c40a4",
                  //OptionSettings: settings // Configuración específica del entorno
                }, function (err, data) {
                  if (err) {
                    console.error('Failed to create cloned environment:', err);

                  }
                  else {
                    res.sendFile(path.join(__dirname, 'login.html')); // cambiar aqui
                    console.log('Environment cloned successfully:', data);

                  }

                });

              }

            });


          }  // cierre else

        });
      });
    }
  });
});


// Start the server
app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
