# Development of a Dashboard for Sensor Analysis and Testing on Raspberry Pi 4 Using Elastic Beanstalk, Node.js, and AWS Cognito

This project involves creating an interactive dashboard that allows for the analysis and testing of the performance of various sensors connected to a Raspberry Pi 4. The following AWS services and technologies will be used:

# Elastic Beanstalk
To deploy and manage the backend infrastructure in a scalable and automated manner.

# Node.js
As the programming language to develop the server logic that will collect and process sensor data

# Cognito

For user authentication and management, ensuring secure and controlled access to the system

# Approach

This approach will integrate the IoT connectivity of the Raspberry Pi 4 with a cloud-based solution, enabling real-time monitoring of sensor performance and facilitating data-driven decision-making.